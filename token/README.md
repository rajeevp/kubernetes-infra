# Create token with cluster admin role
  This needs to be run on the provisioner VM or somebody with existing cluster admin account:

``` bash 

cat << EOF | kubectl apply -f -
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: test-user
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: test-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: test-user
    namespace: kube-system
EOF
```

# fetch the token 

``` bash 
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep test-user | awk '{print $1}')

```

